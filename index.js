// Before using this code, you have to do this.
// npm install pdfkit --save
//-----------------------------------------------------------------------
// ....... //
// Generating pdf //

router.post('/pdfGenrator', function(req, res) {

    var PDFDocument = require('pdfkit'); // add pdfkit module to access it

    var doc = new PDFDocument(); // create instance of PDFDocument

    var path = require('path'); //  add path module to get path

    var filename = req.body.filename; // this file name is get by form

    var content = req.body.content; // this text content is get by form

    doc.y = 320; // this set the document horizontal position

    doc.text(content, 100, 100);

    doc.write(path.resolve(".") + '/PDF/' + filename + '.pdf'); // it create a file that write the document

    res.download(path.resolve(".") + '/PDF/' + filename + '.pdf'); // it download this file

    doc.end(); // document end by the end method

});

// ----------------------------------------------------------------------------
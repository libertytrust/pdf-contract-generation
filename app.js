var PDF = require('pdfkit'); //including the pdfkit module
var fs = require('fs');
var text = 'ANY_TEXT_YOU_WANT_TO_WRITE_IN_PDF_DOC';

var HeadTxt = 'Residential Lease';
var Intro = 'INTRODUCTION';
var Address = '123 Main st';
var Pfirst = 'By this Agreement, made on the ___ day of ____.' +
    'between ____________herein Known as "Landlord" and ______________ herein known as ' +
    'the "tenant".The Landlord agrees to lease the premises located at ' + Address +
    'and more precisely described as follows:';
var Psecond = '<Legal description name of property from HUD-1>';
var Pthird = 'Together with all appurtenances for a term of __ Years to commence at 00:01AM/PM, on the  __ day of ___________ and to end 12:50AM/PM on th __ day of _____________.';
var rent = 'RENT';
var Pfour = 'Tenant agrees to pay, in advance and withut demand, to the Landlord for the demised premises the sum of ($____) per month in advance fo the ___ day of each calendar month beginning on ___day of ___at:______ or other such places as Landlord might designate.';
var Pfive = 'Rent payment shall be made by in-person delivery, by mail,electronic funds transfer, or inter-bank transfer.The method of rend';

doc = new PDF(); //creating a new PDF object
doc.pipe(fs.createWriteStream('output.pdf')); //creating a write stream 
//to write the content on the file system
doc.font('Times-Roman')
    .text(HeadTxt, {
        align: 'center'
    });

doc.text(Intro, {
    align: 'left'
});

doc.text(Pfirst, {
    align: 'justify'
});

doc.text(Psecond, {
    align: 'center'
});

doc.text(Pthird, {
    align: 'justify'
});

doc.text(rent, {
    align: 'left'
});

doc.text(Pfour, {
    align: 'justify'
});

doc.text(Pfive, {
    align: 'justify'
});

doc.image('sample/Initials.png', 200, 300, {
    width: 100,
    height: 100
});

doc.end();